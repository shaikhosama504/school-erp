import os

from setuptools import find_packages, setup

from school_erp import __version__ as version

requirements_file = os.environ.get("REQUIREMENTS_FILE", "requirements.txt")


with open(requirements_file) as f:
    install_requires = f.read().strip().split("\n")


setup(
    name="school_erp",
    version=version,
    description="School ERP",
    author="Osama Shaikh",
    author_email="shaikhosama504@gmail.com",
    packages=find_packages(),
    zip_safe=False,
    include_package_data=True,
    install_requires=install_requires,
)
