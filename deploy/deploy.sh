#backend
curl --insecure -s -X POST https://portainer.drnoorgroup.com/api/webhooks/71b01e41-9ebf-4665-9282-83440c26db04 -o /dev/null

#frontend
curl --insecure -s -X POST https://portainer.drnoorgroup.com/api/webhooks/c1b4d555-f278-4c64-9100-4f182f4bb1b4 -o /dev/null

#migration
curl --insecure -s -X POST https://portainer.drnoorgroup.com/api/webhooks/e76bdd2d-d8b9-4eeb-83d7-742a21c840d5 -o /dev/null

#queue-default
curl --insecure -s -X POST https://portainer.drnoorgroup.com/api/webhooks/0296ca46-88fa-4d04-8908-088be14fba7c -o /dev/null

#queue-long
curl --insecure -s -X POST https://portainer.drnoorgroup.com/api/webhooks/e4d31cfa-7e6c-46bf-b5f0-2be5b96b2754 -o /dev/null

#queue-short
curl --insecure -s -X POST https://portainer.drnoorgroup.com/api/webhooks/2331df03-ce91-453d-8797-8fc8644ba909 -o /dev/null

#scheduler
curl --insecure -s -X POST https://portainer.drnoorgroup.com/api/webhooks/d3f36b75-8c9b-4c12-ae5b-593e44302f42 -o /dev/null

#websocket
curl --insecure -s -X POST https://portainer.drnoorgroup.com/api/webhooks/c3a7d06a-2298-4a4f-8474-a4aff96f0a63 -o /dev/null
