from frappe import _


def get_data():
    return [
        {
            "module_name": "School ERP",
            "type": "module",
            "label": _("School ERP"),
        }
    ]
